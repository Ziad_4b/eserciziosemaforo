package semaforo;
public class Semaforo {
    String colore = "rosso";
    int temporizzazione = 50, timer;

    public Semaforo(int tick){
        this.temporizzazione = tick;
    }

    public void temporizzazione() {
        while (true) {
            if (timer < 0) {
                changeColor();
                System.out.println(toString());
                timer = temporizzazione;
            }
            timer--;
        }
    }

    public String toString() {
        return colore;
    }

    public void changeColor() {
        switch (colore) {
            case "giallo":
                colore = "rosso";
                break;
            case "verde":
                colore = "giallo";
                break;
            case "rosso":
                colore = "verde";
                break;
        }
    }

}